yum update -y
yum install -y yum-utils device-mapper-persistent-data lvm2 epel-release.noarch

# Disable swap
sed -i -E 's|(^.* swap .*$)|# \1|' /etc/fstab
swapoff --all

