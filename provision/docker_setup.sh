yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo

yum install -y \
  docker-ce-18.09.9 \
  docker-ce-cli-18.09.9 \
  containerd.io

mkdir /etc/docker

cat <<EOF > /etc/docker/daemon.json
{
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2",
  "storage-opts": ["overlay2.override_kernel_check=true"]
}
EOF

mkdir -p /etc/systemd/system/docker.service.d

systemctl daemon-reload
systemctl enable docker
systemctl restart docker

