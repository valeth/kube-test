MASTER_ADDRESS="192.168.50.10"

cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF

setenforce 0
sed -i 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config

yum install -y kubelet kubeadm kubectl --disableexcludes=kubernetes

cat <<EOF >  /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF

sysctl --system

echo "KUBELET_EXTRA_ARGS=--node-ip=$MASTER_ADDRESS" >> /etc/default/kubelet

systemctl daemon-reload
systemctl enable kubelet
systemctl restart kubelet

# Pull all required images
kubeadm config images pull

# Bootstrap Kubernetes
#CIDR_ADDRESS="10.244.0.0/16" # Flannel
CIDR_ADDRESS="192.168.0.0/16" # Calico

kubeadm init \
  --pod-network-cidr="$CIDR_ADDRESS" \
  --apiserver-advertise-address="$MASTER_ADDRESS" \
  --apiserver-cert-extra-sans="$MASTER_ADDRESS" \
  --node-name="k8s-master"

echo 'export KUBECONFIG=/etc/kubernetes/admin.conf' >> ~/.bashrc
export KUBECONFIG=/etc/kubernetes/admin.conf

# Container Network Interface (CNI)
#kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/v0.11.0/Documentation/kube-flannel.yml
#kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml
#kubectl apply -f https://raw.githubusercontent.com/projectcalico/canal/master/k8s-install/canal.yaml
kubectl apply -f https://docs.projectcalico.org/v3.10/manifests/calico.yaml

# Dashboard
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v1.10.1/src/deploy/recommended/kubernetes-dashboard.yaml

# NGINX Ingress Controller
#kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/nginx-0.26.1/deploy/static/mandatory.yaml
#kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/nginx-0.26.1/deploy/static/provider/baremetal/service-nodeport.yaml
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/static/mandatory.yaml
#kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/static/provider/baremetal/service-nodeport.yaml

# Bare-Metal Load Balancer
#kubectl apply -f https://raw.githubusercontent.com/google/metallb/v0.8.1/manifests/metallb.yaml

# Allow pods to be scheduled on the control-plane node
kubectl taint nodes --all node-role.kubernetes.io/master-

echo "Copy the following to access the control plane"
echo "----------------------------------------------"
cat /etc/kubernetes/admin.conf