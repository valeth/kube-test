# Kubernetes Test

For experimentation with Kubernetes

## Ports

Range               | Description
------------------- | -----------
2379-2380           | etcd API
6443                | API server
10250               | Kubelet API
10251               | kube-scheduler
10252               | kube-controller-manager
30000...32767       | NodePort services
